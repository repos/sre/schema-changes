from auto_schema.schema_change import SchemaChange

# Copy this file and make adjustments

# Set to None or 0 to skip downtiming
downtime_hours = 4
ticket = 'T367781'

# Don't add set session sql_log_bin=0;
command = """DROP  INDEX af_user ON  /*_*/abuse_filter;
ALTER TABLE  /*_*/abuse_filter
DROP  af_user,
DROP  af_user_text,
CHANGE  af_actor af_actor BIGINT UNSIGNED NOT NULL;
DROP  INDEX afh_user ON  /*_*/abuse_filter_history;
DROP  INDEX afh_user_text ON  /*_*/abuse_filter_history;
ALTER TABLE  /*_*/abuse_filter_history
DROP  afh_user,
DROP  afh_user_text,
CHANGE  afh_actor afh_actor BIGINT UNSIGNED NOT NULL;"""

# Set this to false if you don't want to run on all dbs
# In that case, you have to specify the db in the command and check function.
all_dbs = True

# DO NOT FORGET to set the right port if it's not 3306
# Use None instead of [] to get all direct replicas of master of active dc
replicas = None
section = 's6'

# The check function must return true if schema change is applied
# or not needed, False otherwise.


def check(db):
    columns_abuse_filter = db.get_columns('abuse_filter')
    columns_abuse_filter_history = db.get_columns('abuse_filter_history')

    columns_removed = (
        'af_user' not in columns_abuse_filter and 'afh_user_text' not in columns_abuse_filter_history
    )

    return columns_removed

schema_change = SchemaChange(
    replicas=replicas,
    section=section,
    all_dbs=all_dbs,
    check=check,
    command=command,
    ticket=ticket,
    downtime_hours=downtime_hours
)
schema_change.run()
