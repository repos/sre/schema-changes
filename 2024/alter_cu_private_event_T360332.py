from auto_schema.schema_change import SchemaChange

# Copy this file and make adjustments

# Set to None or 0 to skip downtiming
section = 's6'
ticket = 'T360332'
downtime_hours = 12

# Don't add set session sql_log_bin=0;
command = """ALTER TABLE /*_*/cu_private_event CHANGE cupe_actor cupe_actor BIGINT UNSIGNED DEFAULT 0;"""

# Set this to false if you don't want to run on all dbs
# In that case, you have to specify the db in the command and check function.
all_dbs = True

# DO NOT FORGET to set the right port if it's not 3306
# Use None instead of [] to get all direct replicas of master of active dc
replicas = None

# The check function must return true if schema change is applied
# or not needed, False otherwise.

def check(db):
    return "yes" in db.get_columns('cu_private_event')['cupe_actor']['IS_NULLABLE'].lower()

schema_change = SchemaChange(
    replicas=replicas,
    section=section,
    all_dbs=all_dbs,
    check=check,
    command=command,
    ticket=ticket,
    downtime_hours=downtime_hours
)
schema_change.run()
