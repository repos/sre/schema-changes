from auto_schema.schema_change import SchemaChange

section = "s7changeme"
downtime_hours = 24
ticket = "T348183"

# Don't add set session sql_log_bin=0;
command = """
ALTER TABLE  /*_*/filearchive
CHANGE  fa_size fa_size BIGINT UNSIGNED DEFAULT 0;
ALTER TABLE  /*_*/image
CHANGE  img_size img_size BIGINT UNSIGNED DEFAULT 0 NOT NULL;
ALTER TABLE  /*_*/oldimage
CHANGE  oi_size oi_size BIGINT UNSIGNED DEFAULT 0 NOT NULL;
ALTER TABLE  /*_*/uploadstash
CHANGE  us_size us_size BIGINT UNSIGNED NOT NULL;
"""

# Set this to false if you don't want to run on all dbs
# In that case, you have to specify the db in the command.
all_dbs = True

# DO NOT FORGET to set the right port if it's not 3306
# Use None instead of [] to get all pooled replicas
replicas = None


# Should return true if schema change is applied
def check(db):
    results = []
    if db.get_columns("filearchive")["fa_size"]["COLUMN_TYPE"].lower() == "bigint(20) unsigned":
        results.append(True)
    else:
        results.append(False)
    if db.get_columns("filearchive")["fa_size"]["COLUMN_DEFAULT"] == 0:
        results.append(True)
    else:
        results.append(False)
    return False not in results

if 'changeme' not in section:
    schema_change = SchemaChange(
        replicas=replicas,
        section=section,
        all_dbs=all_dbs,
        check=check,
        command=command,
        ticket=ticket,
        downtime_hours=downtime_hours,
    )
    schema_change.run()
